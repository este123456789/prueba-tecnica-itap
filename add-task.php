<?php 
    include('config/constants.php');
?>

<html>
    <head>
        <title>Prueba PHP Itap - Esteban Villa</title>
        <link rel="stylesheet" href="<?php echo SITEURL; ?>css/style.css" />
    </head>
    
    <body>
    
        <div class="wrapper">
        
        <h1>Prueba tecnica ITAP</h1>
        
        <a class="btn-secondary" href="<?php echo SITEURL; ?>">Inicio</a>
        
        <h3>Agregar nueva tarea</h3>
        
        <p>
            <?php 
            
                if(isset($_SESSION['add_fail']))
                {
                    echo $_SESSION['add_fail'];
                    unset($_SESSION['add_fail']);
                }
            
            ?>
        </p>
        
        <form method="POST" action="">
            
            <table class="tbl-half">
                <tr>
                    <td>Nombre: </td>
                    <td><input type="text" name="task_name"  required="required" /></td>
                </tr>
                
                <tr>
                    <td>Descripción: </td>
                    <td><textarea name="task_description" ></textarea></td>
                </tr>
              
                
                <tr>
                    <td>Fecha de finalización: </td>
                    <td><input type="date" name="deadline" /></td>
                </tr>
                
                <tr>
                    <td><input class="btn-primary btn-lg" type="submit" name="submit" value="Guardar" /></td>
                </tr>
                
            </table>
            
        </form>
        
        </div>
    </body>
    
</html>


<?php 

    //Check whether the SAVE button is clicked or not
    if(isset($_POST['submit']))
    {
        //echo "Button Clicked";
        //Get all the Values from Form
        $task_name = $_POST['task_name'];
        $deadline = $_POST['deadline'];
        $task_description = $_POST['task_description'];
        $list_id = $_POST['list_id'];
        $userid = $_SESSION['userid'];
        //Connect Database
        $conn2 = mysqli_connect(LOCALHOST, DB_USERNAME, DB_PASSWORD) or die(mysqli_error());
        
        //SElect Database
        $db_select2 = mysqli_select_db($conn2, DB_NAME) or die(mysqli_error());
        
        //CReate SQL Query to INSERT DATA into DAtabase
        $sql2 = "INSERT INTO tbl_tasks SET 
            task_name = '$task_name',
            task_description = '$task_description',
            deadline = '$deadline',
            userid = '$userid'
        ";
        

        //Execute Query
        $res2 = mysqli_query($conn2, $sql2);
        
        //Check whetehre the query executed successfully or not
        if($res2==true)
        {
            //Query Executed and Task Inserted Successfully
            $_SESSION['add'] = "Tarea agregada correctamente.";
            
            //Redirect to Homepage
            header('location:'.SITEURL);
            
        }
        else
        {
            //FAiled to Add TAsk
            $_SESSION['add_fail'] = "Error agregando la tarea";
            //Redirect to Add TAsk Page
            header('location:'.SITEURL.'add-task.php');
        }
    }

?>




































