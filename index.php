<?php 
include('config/constants.php');
?>

<html>

<head>
    <title>Prueba PHP Itap - Esteban Villa</title>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo SITEURL; ?>css/style.css" />
</head>

<body>


    <div class="wrapper">

    <?php 

    if(!isset($_SESSION['usr'])){ ?>
        <!-- Modal login-->
        <div id="myModal" class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Ingresar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <p>Ingresa el usuario y la contraseña a continuacion</p>
            <form action="#" method="post" class="loginForm">
                <label>
                    Usuario:
                    <input type="text" name="username" id="username">

                </label>
                <label>
                    Contraseña:
                    <input type="password" name="pwd" id="pwd">

                </label>

            </form>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="log">Ingresar</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>
<?php };?>


<h1>Prueba tecnica ITAP</h1>


<!-- Menu Starts Here -->
<div class="menu">

    <a href="<?php echo SITEURL; ?>">Inicio</a>




</div>
<!-- Menu Ends Here -->

<!-- Tasks Starts Here -->

<p>
    <?php 

    if(isset($_SESSION['add']))
    {
        echo $_SESSION['add'];
        unset($_SESSION['add']);
    }

    if(isset($_SESSION['delete']))
    {
        echo $_SESSION['delete'];
        unset($_SESSION['delete']);
    }

    if(isset($_SESSION['update']))
    {
        echo $_SESSION['update'];
        unset($_SESSION['update']);
    }


    if(isset($_SESSION['delete_fail']))
    {
        echo $_SESSION['delete_fail'];
        unset($_SESSION['delete_fail']);
    }

    ?>
</p>

<div class="all-tasks">

    <?php 

    if(isset($_SESSION['usr'])){ ?>
    <a class="btn-primary" href="<?php SITEURL; ?>add-task.php">Agregar tarea</a>
    <?php }
    else { ?>

    <a class="btn-primary" data-toggle="modal" data-target="#myModal" href="#">Ingresar</a>

<?php };?>


    <?php 

    if(isset($_SESSION['usr'])){ ?>

    <table class="tbl-full">

        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Fecha de finalización</th>
            <th>Tiempo reportado</th>
            <th>Ultima fecha que se agrego hora</th>
            <th>Opciones</th>
        </tr>

        <?php 

                //Connect Database
        $conn = mysqli_connect(LOCALHOST, DB_USERNAME, DB_PASSWORD) or die(mysqli_error());

                //Select Database
        $db_select = mysqli_select_db($conn, DB_NAME) or die(mysqli_error());

                //Create SQL Query to Get DAta from Databse
        $sql = "SELECT * FROM tbl_tasks WHERE userid = '".$_SESSION['userid']."'";

                //Execute Query
        $res = mysqli_query($conn, $sql);

                //CHeck whether the query execueted o rnot
        if($res==true)
        {
                    //DIsplay the Tasks from DAtabase
                    //Dount the Tasks on Database first
            $count_rows = mysqli_num_rows($res);

                    //Create Serial Number Variable
            $sn=1;

                    //Check whether there is task on database or not
            if($count_rows>0)
            {
                        //Data is in Database
                while($row=mysqli_fetch_assoc($res))
                {
                    $task_id = $row['task_id'];
                    $task_name = $row['task_name'];
                    $priority = $row['priority'];
                    $deadline = $row['deadline'];

                    $sql1 = "SELECT SUM(time) as tiempo FROM tbl_time WHERE idtask = '".$task_id."'  ";
                    $sql2 = "SELECT date as tiempo FROM tbl_time WHERE idtask = '".$task_id."'  ";


//Execute Query
                    $res1 = mysqli_query($conn, $sql1);
                    $res2 = mysqli_query($conn, $sql2);

//Check whether the query executed or not
                    $data1 = $res1->fetch_row();
                    $data2 = $res2->fetch_row();

                    $hours = 0;


                    if(!empty($data1)){

                        $total_hours = 0;


                        foreach ($data1 as $hor) {

                            $total_hours = $total_hours + $hor;
                        }

                    }





                    ?>

                    <tr>
                        <td><?php echo $sn++; ?>. </td>
                        <td><?php echo $task_name; ?></td>
                        <td><?php echo $deadline; ?></td>
                        <td><?php echo $total_hours;?> Horas</td>
                        <td><?php echo $data2[0];?></td>
                        <td>
                            <a href="<?php echo SITEURL; ?>update-task.php?task_id=<?php echo $task_id; ?>">Actualizar  </a>
                            <br>
                            <a href="<?php echo SITEURL; ?>delete-task.php?task_id=<?php echo $task_id; ?>">Borrar  </a>
                            <br>
                            <a href="<?php echo SITEURL; ?>add-time.php?task_id=<?php echo $task_id; ?>">Agregar tiempo</a>
                        </td>
                    </tr>

                    <?php
                }
            }
            else
            {
                        //No data in Database
                ?>

                <tr>
                    <td colspan="5">No existen tareas agregadas.</td>
                </tr>

                <?php
            }
        }

        ?>



    </table>



<?php };?>

</div>

<!-- Tasks Ends Here -->
</div>


<!--Custom´s Scripts-->

<script src="js/custom.js"></script>


</body>

</html>