# PRUEBA TÉCNICA ANALISTA DESARROLLADOR
Manejador de tareas.


## Tecnologías usadas
1. PHP
2. MySQL Database
3. HTML
4. CSS
5. jQuery

## ¿Como instalar?

### Pre-Requisitos:

1. Download and Install XAMPP

[Clic en el enlace](https://www.apachefriends.org/index.html)

2. Instalar un editor de texto (Sublime Text or Visual Studio Code or Atom or Brackets)

### Instalación

1. Descargar el archivo .zip o clonar repositorio
2. Mover el archivo a la carpeta raiz
```
Disco local C: -> xampp -> htdocs -> 'myProyecto'
```
*El disco local depende de la carpeta donde este instalado xampp*

3. Abrir XAMPP control paner e iniciar los servcios apache y mysql 

4. Importar la base de datos ubicada en la carpeta db

a. abrir 'phpmyadmin' en su navegador
b. Crear la base de datos
c. Importar el archivo .sql ubicado en la carpeta /db

5. Configuracion de la base de datos

Ir a la carpeta /config y en el archivo constants.php se debe agregar los datos de su conexion a la base de datos, contraseña y ruta del sitio web.
```php
<?php 
//Start Session
session_start();

//Create Constants to save Database Credentials
define('LOCALHOST', 'localhost');
define('DB_USERNAME', 'root'); //Your Database username instead of 'root'
define('DB_PASSWORD', ''); //Your Database Password instead of null/empty
define('DB_NAME', 'tasksdb'); //Your Database Name if it's not 'task_manager'

define('SITEURL', 'http://localhost/itap_empleados/'); //Update the home URL of the project if you have changed port number or it's live on server

?>
```

6. Ahora abre el proyecto en su navegador y vealo funcionando :) .

## Realizado por Esteban Villa R.
1. Correo: - estebanvr1010@gmail.com

