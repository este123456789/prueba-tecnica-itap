<?php 
include('config/constants.php');
    //
$task_id = $_GET['task_id'];


if(isset($_GET['task_id']))
{
        //Get the Values from DAtabase
	$task_id = $_GET['task_id'];

        //Connect Database
	$conn = mysqli_connect(LOCALHOST, DB_USERNAME, DB_PASSWORD) or die(mysqli_error());

        //Select Database
	$db_select = mysqli_select_db($conn, DB_NAME) or die(mysqli_error());

        //SQL Query to Get the detail of selected task
	$sql = "SELECT * FROM tbl_tasks WHERE task_id=$task_id";

        //Execute Query
	$res = mysqli_query($conn, $sql);

        //Check if the query executed successfully or not
	if($res==true)
	{
            //Query <br />Executed
		$row = mysqli_fetch_assoc($res);

            //Get the Individual Value
		$task_name = $row['task_name'];
		$task_description = $row['task_description'];
		$list_id = $row['list_id'];
		$priority = $row['priority'];
		$deadline = $row['deadline'];
	}
}
else
{
        //Redirect to Homepage
	header('location:'.SITEURL);
}
?>

<html>
<head>
	<title>Prueba PHP Itap - Esteban Villa</title>
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo SITEURL; ?>css/style.css" />
	<link rel="stylesheet" href="<?php echo SITEURL; ?>css/style.css" />
</head>

<body>

	<div class="wrapper">

		<h1>Prueba tecnica ITAP</h1>

		<p>
			<a class="btn-secondary" href="<?php echo SITEURL; ?>">Home</a>
		</p>

		<h3>Agregar tiempo a la tarea</h3>

		<p>
			<?php 
			if(isset($_SESSION['update_fail']))
			{
				echo $_SESSION['update_fail'];
				unset($_SESSION['update_fail']);
			}
			?>
		</p>

		<form method="POST" action="">

			<table class="tbl-half">
				<tr>
					<td>Nombre: </td>
					<td><input type="text" name="task_name" value="<?php echo $task_name; ?>" required="required" readonly /></td>
				</tr>

				<tr>
					<td>Descripción: </td>
					<td>
						<textarea name="task_description" readonly>
							<?php echo $task_description; ?>
						</textarea>
					</td>
				</tr>



				<tr>
					<td>Agregar tiempo</td>
					<td>

						<select name="time" id="time" data-idtask="<?php echo $task_id;?>">
							<option value="1">1 Hora</option>
							<option value="2">2 Hora</option>
							<option value="3">3 Hora</option>
							<option value="4">4 Hora</option>
							<option value="6">6 Hora</option>
							<option value="7">7 Hora</option>
							<option value="8">8 Hora</option>
						</select>
					</td>
				</tr>


				<tr>
					<td><input id="sendtime"class="btn-primary btn-lg" type="submit" name="submit" value="Agregar tiempo" disabled="disabled" /></td>
				</tr>

			</table>

		</form>
	</div>

	<!--Custom´s Scripts-->

	<script src="js/custom.js"></script>
</body>
</html>


<?php 

    //Check if the button is clicked
if(isset($_POST['submit']))
{
        //echo "Clicked";

        //Get the CAlues from Form
	$task_name = $_POST['task_name'];
	$task_description = $_POST['task_description'];
	$list_id = $_POST['list_id'];
	$priority = $_POST['priority'];
	$deadline = $_POST['deadline'];
	$time = $_POST['time'];

        //Connect Database
	$conn3 = mysqli_connect(LOCALHOST, DB_USERNAME, DB_PASSWORD) or die(mysqli_error());

        //SElect Database
	$db_select3 = mysqli_select_db($conn3, DB_NAME) or die(mysqli_error());

        //CREATE SQL QUery to Update TAsk
	$sql3 = "INSERT INTO `tbl_time`
	(`idtask`, `time`) 
	VALUES ('$task_id','$time')";

        //Execute Query
	$res3 = mysqli_query($conn3, $sql3);

        //CHeck whether the Query Executed of Not
	if($res3==true)
	{
            //Query Executed and Task Updated
		$_SESSION['update'] = "Tiempo agregado a la tarea.";

            //Redirect to Home Page
		header('location:'.SITEURL);
	}
	else
	{
            //FAiled to Update Task
		$_SESSION['update_fail'] = "Failed to Update Task";

            //Redirect to this Page
		header('location:'.SITEURL.'update-task.php?task_id='.$task_id);
	}


}

?>
