$(document).ready(function(){



	$('#myModal').modal('show');


	$(document).on('click','#log',function(){
		var usr = $('#username').val();
		var pwd = $('#pwd').val();
		$.ajax({

			url:'verify-user.php',
			method: 'POST',
			dataType : 'json',
			data: 	
			{
				'username': usr,
				'pwd' : pwd
			}
			,
			success: function(data){

				if(data===true){
					alertify.success('Bienvenido '+usr);
				}
				else {
					alertify.error('Error usuario y contraseña no son correctos');
				}

			}


		});

	});



	$(document).on('input','#time',function(){

		var idtask = $(this).data('idtask');
		var time = $(this).val();

		$.ajax({

			url:'verify-time.php',
			method: 'POST',
			dataType : 'json',
			data: 	
			{
				'idtask': idtask,
				'time' : time
			},
			success: function(data){
				if(data!==true){
					alertify.error('Error: la tarea supera las 8 horas establecidas como maximo.');
				}
				else {
					$('#sendtime').removeAttr('disabled');
				}
			}


		});

	});



});




